using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour
{
    [SerializeField] float invokeDelay = 2f;
    [SerializeField] AudioClip crashSFX;
    [SerializeField] AudioClip successSFX;

    [SerializeField] ParticleSystem crashParticles;
    [SerializeField] ParticleSystem successParticles;
    
    AudioSource audioSource;
    bool isTransitioning;
    bool collisionDisabled;
    private void Start() {
        audioSource = GetComponent<AudioSource>();
    }
    private void Update() {
        RespondToDebugKeys();
    }

    void RespondToDebugKeys(){
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadNextLevel();            
        }

        else if (Input.GetKeyDown(KeyCode.C))
        {
            collisionDisabled = !collisionDisabled; // toggle collision            
        }
    }
   private void OnCollisionEnter(Collision other) {

        if (isTransitioning || collisionDisabled) return;
        
        switch (other.gameObject.tag)
        {
            case "Finish":
                StartLoadingNextLevel();
                break;

            case "Friendly":

                break;   
            
            default:
                StartCrashSequence();
                break;
        }
   }

   void StartCrashSequence(){
        isTransitioning = true;
        audioSource.Stop();
        GetComponent<Movement>().enabled = false;     
        audioSource.PlayOneShot(crashSFX, 0.5f);
        crashParticles.Play();              
        Invoke("ReloadLevel", invokeDelay);
   }

   public void StartLoadingNextLevel(){
        isTransitioning = true;
        audioSource.Stop();
        GetComponent<Movement>().enabled = false;
        audioSource.PlayOneShot(successSFX);
        successParticles.Play();
        Invoke("LoadNextLevel", invokeDelay);
   }

   void ReloadLevel(){
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
   }

   void LoadNextLevel(){
        int numberOfScenes = SceneManager.sceneCount;
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;

        if (currentSceneIndex < numberOfScenes)
        {
            SceneManager.LoadScene(currentSceneIndex + 1);
        }
        else
            SceneManager.LoadScene(0);        
   }
}
