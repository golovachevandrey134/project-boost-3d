using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] float rotationPower = 100f;
    [SerializeField] float thrustPower = 100f;
    [SerializeField] AudioClip engineThrust;
    [SerializeField] ParticleSystem mainThrusterParticles;
    [SerializeField] ParticleSystem leftThrusterParticles;
    [SerializeField] ParticleSystem rightThrusterParticles;

    Rigidbody rbody;
    AudioSource audioSource;
    void Start()
    {
        rbody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        ProcessThrust();
        ProcessRotation();
    }

    private void ProcessThrust()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            StartThrusting();
        }
        else
        {
            StopThrusting();
        }

    }
    
    private void ProcessRotation()
    {
        if (Input.GetKey(KeyCode.A))
        {
            RotateCounterClockwise();
        }

        else if (Input.GetKey(KeyCode.D))
        {
            RotateClockwise();
        }
        else
        {
            rbody.freezeRotation = false;
            StopRotating();
        }
    }
    private void StartThrusting()
    {
        rbody.AddRelativeForce(Vector3.up * thrustPower * Time.deltaTime);

        if (!mainThrusterParticles.isPlaying)
        {
            mainThrusterParticles.Play();
        }

        if (!audioSource.isPlaying)
        {
            audioSource.PlayOneShot(engineThrust);
        }
    }
    private void StopThrusting()
    {
        audioSource.Stop();
        mainThrusterParticles.Stop();
    }   

    private void RotateClockwise()
    {
        ApplyRotation(-rotationPower);
        if (!leftThrusterParticles.isPlaying)
        {
            leftThrusterParticles.Play();
        }
    }

    private void RotateCounterClockwise()
    {
        ApplyRotation(rotationPower);
        if (!rightThrusterParticles.isPlaying)
        {
            rightThrusterParticles.Play();
        }
    }
    private void StopRotating()
    {
        rightThrusterParticles.Stop();
        leftThrusterParticles.Stop();
    }

    private void ApplyRotation(float rotationThisFrame)
    {
        rbody.freezeRotation = true; //freezing engine rotation so we can manually rotate
        transform.Rotate(Vector3.forward * rotationThisFrame * Time.deltaTime);
    }
}
